package lt.justas;

import lt.config.HibernateConfiguration;
import lt.dataload.InitialDataLoad;
import lt.model.Customer;
import lt.model.Manufacturer;
import lt.model.Vaccine;
import lt.print.SystemChoicePrinter;
import lt.services.CustomerService;
import lt.services.ManufacturerService;
import lt.services.VaccineService;
import lt.util.PrintUtil;
import org.hibernate.SessionFactory;

import java.util.List;

public class Main {
    private static final String MANUFACTURER_FILE = "src/main/resources/gamintojas.csv";
    private static final String VACCINES_FILE = "src/main/resources/vakcinos.csv";

    private static final SessionFactory CONNECTION = HibernateConfiguration.getSessionFactory();
    private static final ManufacturerService MANUFACTURER_SERVICE = new ManufacturerService(CONNECTION);
    private static final VaccineService VACCINE_SERVICE = new VaccineService(CONNECTION);
    private static final CustomerService CUSTOMER_SERVICE = new CustomerService(CONNECTION);


    public static void main(String[] args) {
        loadInitialData();
        doAction();
    }

    private static void doAction() {
        int choice = SystemChoicePrinter.mainChoice();
        switch (choice) {
            case 0:
                return;
            case 1:
                CUSTOMER_SERVICE.registerCustomer();
                doAction();
            case 2:
                List<Vaccine> vaccines = VACCINE_SERVICE.getAllNotUsedVaccines();
                vaccines.forEach(PrintUtil::printBasicInfoAboutVaccine);
                doAction();
            case 3:
                VACCINE_SERVICE.useVaccine(CUSTOMER_SERVICE::findById);
                doAction();
            case 4:
                List<Customer> customers = CUSTOMER_SERVICE.showAllCustomersWithoutVaccines();
                customers.forEach(PrintUtil::printBasicInfoAboutCustomer);
                doAction();
            case 5:
                List<Customer> customersWith = CUSTOMER_SERVICE.findByVaccineCount();
                customersWith.forEach(PrintUtil::printBasicInfoAboutCustomer);
                doAction();
            case 6:
                List<Vaccine> vaccinesOfCustomer = VACCINE_SERVICE.showCustomerVaccineHistory();
                vaccinesOfCustomer.forEach(PrintUtil::printDetailInfoAboutVaccines);
                doAction();
        }
    }

    private static void loadInitialData() {
        System.out.println("Making initial Data Load....");
        loadManufactures();
        loadVaccines();
    }

    private static void loadManufactures() {
        System.out.println("Loading manufactors from file " + MANUFACTURER_FILE);
        List<Manufacturer> manufacturerList = InitialDataLoad.loadManufacturersFromFile(MANUFACTURER_FILE);
        manufacturerList.forEach(MANUFACTURER_SERVICE::saveIntoDatabase);

        System.out.println("Manufacturers were saved successfully");
    }

    private static void loadVaccines() {
        System.out.println("Loading vaccines from file " + VACCINES_FILE);

        List<Vaccine> vaccines = InitialDataLoad.loadAllVaccinesFromFile(
                VACCINES_FILE,
                MANUFACTURER_SERVICE::findById
        );

        vaccines.forEach(VACCINE_SERVICE::saveIntoDatabase);
        System.out.println("Vaccines were saved successfully");
    }

}
