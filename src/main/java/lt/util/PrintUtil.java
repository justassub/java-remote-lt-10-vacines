package lt.util;

import lt.model.Customer;
import lt.model.Vaccine;

public class PrintUtil {

    public static void printBasicInfoAboutVaccine(Vaccine vaccine) {
        System.out.printf("%d %s %s %s \n",
                vaccine.getId(),
                vaccine.getName(),
                vaccine.getManufacturer().getCountry(),
                vaccine.getManufacturer().getCountry()
        );
    }

    public static void printDetailInfoAboutVaccines(Vaccine vaccine) {

        System.out.printf("%d %s %s %s \n",
                vaccine.getId(),
                vaccine.getName(),
                vaccine.getManufacturer().getCountry(),
                vaccine.getUsedDate()
        );
    }

    public static void printBasicInfoAboutCustomer(Customer customer) {
        System.out.printf("%d %s %s %d \n",
                customer.getId(),
                customer.getName(),
                customer.getSurname(),
                customer.getAge()
        );
    }
}
