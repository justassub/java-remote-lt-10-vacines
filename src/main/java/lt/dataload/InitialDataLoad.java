package lt.dataload;

import lt.model.Manufacturer;
import lt.model.Vaccine;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class InitialDataLoad {
    private static final String CSV_SEPARATOR = ",";

    public static List<Manufacturer> loadManufacturersFromFile(String file) {
        try {
            return Files.lines(Paths.get(file))
                    .map(InitialDataLoad::createManufacturerFromLine)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.printf("File %s was not found \n", file);
            return Collections.emptyList();
        }
    }

    public static List<Vaccine> loadAllVaccinesFromFile(String file, Function<Long, Manufacturer> getManufacturerById) {
        try {
            return Files.lines(Paths.get(file))
                    .map(line -> createVaccineFromLine(line, getManufacturerById))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.printf("File %s was not found \n", file);
            return Collections.emptyList();
        }
    }

    private static Manufacturer createManufacturerFromLine(String line) {
        String[] data = line.split(CSV_SEPARATOR);
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName(data[0]);
        manufacturer.setCountry(data[1]);
        return manufacturer;
    }

    private static Vaccine createVaccineFromLine(String line, Function<Long, Manufacturer> getManufacturerById) {
        String[] data = line.split(CSV_SEPARATOR);
        Vaccine vaccine = new Vaccine();
        vaccine.setName(data[0]);
        String idAsString = data[1];
        Manufacturer manufacturer = getManufacturerById.apply(Long.parseLong(idAsString));
        vaccine.setManufacturer(manufacturer);
        return vaccine;
    }

}
