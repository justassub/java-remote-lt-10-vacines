package lt.services;

import lt.model.Customer;
import lt.model.Vaccine;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

public class VaccineService {
    private final Scanner scanner = new Scanner(System.in);
    private SessionFactory sessionFactory;

    public VaccineService(SessionFactory connection) {
        this.sessionFactory = connection;
    }

    public void saveIntoDatabase(Vaccine vaccine) {
        Session session = this.sessionFactory.openSession();
        session.save(vaccine);
        session.close();
    }

    public List<Vaccine> getAllNotUsedVaccines() {
        Session session = this.sessionFactory.openSession();
        return session.createQuery("select v from Vaccine v where v.used = false ", Vaccine.class).list();
    }

    public void useVaccine(Function<Long, Customer> customerFunction) {
        System.out.println("Iveskite nepanaudotos vakcinos id: ");
        Long vaccineId = scanner.nextLong();
        Vaccine vaccine = findById(vaccineId);
        if (vaccine == null) {
            System.out.println("Vakcina nebuvo rasta pagal id: " + vaccineId);
            return;
        }
        if (vaccine.isUsed()) {
            System.out.println("Vakcina jau buvo panaudota " + vaccine.getUsedDate());
            return;
        }
        System.out.println("Iveskite paciento id: ");
        Long customerId = scanner.nextLong();


        Customer customer = customerFunction.apply(customerId);
        if (customer == null) {
            System.out.println("Klientas nebuvo rastas pagal id " + customerId);
            return;
        }
        vaccine.setUsed(true);
        vaccine.setUsedDate(LocalDateTime.now());
        vaccine.setCustomer(customer);
        updateVaccine(vaccine);
    }

    private void updateVaccine(Vaccine vaccine) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(vaccine);
        session.getTransaction().commit();
        session.close();
    }

    public List<Vaccine> showCustomerVaccineHistory() {
        System.out.println("Iveskite paciento id: ");
        Long customerId = scanner.nextLong();

        return findByCustomer(customerId);
    }

    private List<Vaccine> findByCustomer(Long customerId) {
        Session session = this.sessionFactory.openSession();

        return session.createQuery(
                        "select v from Vaccine  v where v.customer.id =:customerId order by v.usedDate DESC",
                        Vaccine.class)
                .setParameter("customerId", customerId)
                .list();
    }

    public Vaccine findById(Long id) {
        Session session = this.sessionFactory.openSession();

        return session.find(Vaccine.class, id);
    }
}
