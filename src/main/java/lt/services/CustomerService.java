package lt.services;

import lt.model.Customer;
import lt.model.Vaccine;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Scanner;

public class CustomerService {
    private final Scanner scanner = new Scanner(System.in);
    private final SessionFactory sessionFactory;


    public CustomerService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void registerCustomer() {
        System.out.println("Iveskite kliento vardą: ");
        String name = scanner.next();
        System.out.println("Iveskite kliento pavardę: ");
        String surname = scanner.next();

        System.out.println("Iveskite kliento amžių: ");
        int age = scanner.nextInt();

        Customer customer = new Customer();
        customer.setName(name);
        customer.setSurname(surname);
        customer.setAge(age);

        if (isCustomerValid(customer)) {
            saveIntoDatabase(customer);
        }

        System.out.println("Pacientas buvo išsaugotas sėkminga");

    }

    private boolean isCustomerValid(Customer customer) {
        if (customer.getName().matches(".*[0-9].*")) {
            System.out.println("Vardas negali tureti skaiciaus.");
            return false;
        }
        if (customer.getSurname().matches(".*[0-9].*")) {
            System.out.println("Pavardė negali tureti skaiciaus.");
            return false;
        }

        if (customer.getAge() < 14) {
            System.out.println("Paciento amzius privalo but daugiau nei 14m");
            return false;
        }

        return true;
    }

    public List<Customer> showAllCustomersWithoutVaccines() {
        return showCustomersByVaccineCount(0);
    }

    public List<Customer> findByVaccineCount() {
        System.out.println("Iveskite pagal kiek vakcinu norite ieskoti");
        int count = scanner.nextInt();
        return showCustomersByVaccineCount(count);
    }

    public List<Customer> showCustomersByVaccineCount(int count) {
        Session session = this.sessionFactory.openSession();
        return session.createQuery("Select c from Customer  c where c.vaccines.size = :vaccineCount", Customer.class)
                .setParameter("vaccineCount", count)
                .list();
    }

    private void saveIntoDatabase(Customer customer) {
        Session session = this.sessionFactory.openSession();
        session.save(customer);
        session.close();
    }

    public Customer findById(Long customerId) {
        Session session = this.sessionFactory.openSession();
        return session.find(Customer.class, customerId);
    }
}
