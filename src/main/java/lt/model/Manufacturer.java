package lt.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String country;

    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL)
    private List<Vaccine> vaccines = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void addVaccine(Vaccine vaccine) {
        this.vaccines.add(vaccine);
        vaccine.setManufacturer(this);
    }

    @Override
    public String toString() {
        return "Manufacturer{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", country='" + country + '\'' +
               '}';
    }
}
