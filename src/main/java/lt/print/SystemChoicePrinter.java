package lt.print;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SystemChoicePrinter {
    private final static Scanner SCANNER = new Scanner(System.in);

    private static final Set<Integer> VALID_CHOICES = new HashSet<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6));

    public static int mainChoice() {
        System.out.println("Ka noretumėte padaryti : ");
        System.out.println("0 - uždaryti programą");
        System.out.println("1 - užregistruoti pacientą");
        System.out.println("2 - Pamatyti nepanaudotas vakcinas");
        System.out.println("3 - Paskiepyti pacientą");
        System.out.println("4 - Parodyti pacientus be skiepų");
        System.out.println("5 - Parodyti pacientus pagal skiepu skaičių");
        System.out.println("6 - Parodyti paciento skiepų istoriją");

        try {
            int choice = SCANNER.nextInt();
            if (VALID_CHOICES.contains(choice)) {
                return choice;
            }
        } catch (Exception e) {
            System.out.println("Neteisinga ivestis");
            return 0;
        }
        System.out.println("Neteisinga ivestis");
        return mainChoice();
    }
}
